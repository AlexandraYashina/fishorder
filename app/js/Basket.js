class Basket {
    constructor(idBasket) {
        this.id = idBasket;
        this.fullamount = 0;
        this.basketItems = [];
        this.getItems();
        this.getFullCountOfProducts();
    }

    getItems() {
        let appendId = `#${this.id}_items`;

        $.ajax({
            type: 'GET',
            url: 'https://fishorder-v7m5.firebaseio.com/items.json',
            context: this,
            dataType: 'json',
            success: function (data) {
                let basketData = $('<div />', {
                    id: 'basket_data'
                });

                this.fullamount = data.fullamount;
                if (data.basket) {
                    for (let i = 0; i < data.basket.length; i++) {
                        this.basketItems.push(data.basket[i]);
                    };
                };

                basketData.append(`<h3>Всего товаров: ${this.basketItems.length}</h3>`);
                basketData.append(`<h3>Общая стоимость: ${this.fullamount}</h3>`);

                basketData.appendTo(appendId);
                this.renderTable(this.basketItems);
                $('#smallBasket').html(`Товаров: ${this.getFullCountOfProducts()}`);
            },
            error: function (err) {
                console.log('Ошибка', err);
            }
        });
    }

    render($jQuerySelector) {
        let basketDiv = $('<div />', {
            id: this.id,
            text: 'Корзина'
        });

        let basketItemsDiv = $('<div />', {
            id: `${this.id}_items`
        });

        basketItemsDiv.appendTo(basketDiv);
        basketDiv.appendTo($jQuerySelector);
    }

    addProduct(idProduct, price, name) {
        let productItem = {
            id_product: idProduct,
            price,
            name,
            amount: 1
        };

        for (let i = 0; i < this.basketItems.length; i++) {
            if (this.basketItems[i].id_product == idProduct) {
                this.basketItems[i].amount++;
                this.fullamount += this.basketItems[i].price;
                break;
            }
            if ((this.basketItems[i].id_product != idProduct) && (i == (this.basketItems.length - 1))) {
                this.basketItems.push(productItem);
                this.fullamount += price;
                break;
            }
        }
        if (this.basketItems.length == 0) {
            this.basketItems.push(productItem);
            this.fullamount += price;
        }


        let myBasket = {
            "result": 1,
            "basket": this.basketItems,
            "fullamount": this.fullamount
        };

        let self = this;

        $.ajax({
            type: 'PUT',
            url: 'https://fishorder-v7m5.firebaseio.com/items.json',
            data: JSON.stringify(myBasket),
            dataType: 'json',
            success: function (data) {
                self.refresh();
                $('#smallBasket').html(`Товаров: ${self.getFullCountOfProducts()}`);
            }
        });
    }

    decreaseProduct(idProduct) {
        for (let i = 0; i < this.basketItems.length; i++) {
            if (this.basketItems[i].id_product == idProduct) {

                if (this.basketItems[i].amount == 1) {
                    this.fullamount -= this.basketItems[i].price;
                    this.basketItems.splice(i, 1);
                    break;
                }
                if (this.basketItems[i].amount > 1) {
                    this.basketItems[i].amount--;
                    this.fullamount -= this.basketItems[i].price;
                    break;
                }
            }
        }

        let myBasket = {
            "result": 1,
            "basket": this.basketItems,
            "fullamount": this.fullamount
        };

        let self = this;

        $.ajax({
            type: 'PUT',
            url: 'https://fishorder-v7m5.firebaseio.com/items.json',
            data: JSON.stringify(myBasket),
            dataType: 'json',
            success: function (data) {
                self.refresh();
                $('#smallBasket').html(`Товаров: ${self.getFullCountOfProducts()}`);
            }
        });
    }

    removeProduct(idProduct) {
        for (let i = 0; i < this.basketItems.length; i++) {
            if (this.basketItems[i].id_product == idProduct) {
                this.fullamount -= (this.basketItems[i].price * this.basketItems[i].amount);
                this.basketItems.splice(i, 1);
            }
        }

        let myBasket = {
            "result": 1,
            "basket": this.basketItems,
            "fullamount": this.fullamount
        };

        let self = this;

        $.ajax({
            type: 'PUT',
            url: 'https://fishorder-v7m5.firebaseio.com/items.json',
            data: JSON.stringify(myBasket),
            dataType: 'json',
            success: function (data) {
                self.refresh();
                $('#smallBasket').html(`Товаров: ${self.getFullCountOfProducts()}`);
            }
        });
    }

    getFullCountOfProducts() {
        let fullCountOfProducts = 0;
        for (let i = 0; i < this.basketItems.length; i++) {
            fullCountOfProducts += this.basketItems[i].amount;
        }
        return fullCountOfProducts;
    }

    renderTable() {
        let self = this;
        let basketData = $('#basket_data');
        if (this.basketItems.length > 0) {
            let table = $('<table />', {
                class: 'orderTable'
            });
            let thead = $('<thead />');
            let tr = $('<tr />');
            let th1 = $('<th />', {
                text: 'Наименование товара'
            });
            let th2 = $('<th />', {
                text: 'Цена'
            });
            let th3 = $('<th />', {
                text: 'Количество'
            });
            let th4 = $('<th />', {
                text: 'Итого'
            });
            let th5 = $('<th />', {
                text: 'Удалить',

            });

            th1.appendTo(tr);
            th2.appendTo(tr);
            th3.appendTo(tr);
            th4.appendTo(tr);
            th5.appendTo(tr);
            tr.appendTo(thead);
            thead.appendTo(table);
            table.appendTo(basketData);

            for (let i = 0; i < this.basketItems.length; i++) {
                let sum = this.basketItems[i].amount * this.basketItems[i].price;

                let table = $('.orderTable');
                let tr = $('<tr />');
                let td1 = $('<td />', {
                    class: 'name',
                    'data-id': this.basketItems[i].id_product,
                    text: this.basketItems[i].name
                });
                let td2 = $('<td />', {
                    text: this.basketItems[i].price
                });

                let td3 = $('<td />', {
                    text: this.basketItems[i].amount
                });
                let minusBtn = $('<input />', {
                    type: 'button',
                    class: 'button-minus deletegood',
                    value: '—'
                });
                let plusBtn = $('<input />', {
                    type: 'button',
                    class: 'button-plus buygood',
                    value: '+'
                });
                let td4 = $('<td />', {
                    text: sum
                });
                let td5 = $('<td />', {
                    text: 'Удалить',
                    class: 'delete-row',
                    'data-id': this.basketItems[i].id_product
                });
                td1.appendTo(tr);
                td2.appendTo(tr);
                minusBtn.prependTo(td3);
                plusBtn.appendTo(td3);
                td3.appendTo(tr);
                td4.appendTo(tr);
                td5.appendTo(tr);
                tr.appendTo(table);
                thead.appendTo(table);
                $(plusBtn).on('click', function (event) {
                    self.addProduct(self.basketItems[i].id_product, self.basketItems[i].price, self.basketItems[i].name);
                });

                $(minusBtn).on('click', function (event) {
                    self.decreaseProduct(self.basketItems[i].id_product);
                });
                $(td5).on('click', function (event) {
                    let idProduct = parseInt($(this).attr('data-id'));
                    self.removeProduct(idProduct);
                });
            }
        }
    }

    refresh() {
        let basketData = $('#basket_data');
        basketData.empty();
        basketData.append(`<h3>Всего товаров: ${this.basketItems.length}</h3>`);
        basketData.append(`<h3>Общая стоимость: ${this.fullamount}</h3>`);
        this.renderTable(this.basketItems);
    }
}