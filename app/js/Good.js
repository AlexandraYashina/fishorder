class Good {
    constructor(id, price, name, image) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.image = image;
    }
    render($jQueryElement, basket) {
        let productContainer = $('<div />', {
            class: 'productCard productCard_position'
        });
        let productTitle = $('<p />', {
            text: this.name,
            class: 'productCard__title productCard__title_margin'
        });
        let productPrice = $(`<p class="productPrice">Стоимость: <span class="productPrice__number productPrice__number_margin" data-id="${this.id}">${this.price}</span> руб.</p>`);

        let productImage = $(`<img src="${this.image}" class="productCard__image" alt="${this.name}">`);
               let productBtnAdd = $('<button />', {
                   class: 'buyProduct',
                   text: 'Добавить в корзину',
                   'data-id': this.id
               });
        
               let productBtnDelete = $('<button />', {
                   class: 'deleteProduct',
                   text: 'Удалить',
                   'data-id': this.id
               });

        productTitle.appendTo(productContainer);
        productPrice.appendTo(productContainer);
        productImage.appendTo(productContainer);
        productBtnAdd.appendTo(productContainer);
        productBtnDelete.appendTo(productContainer);
        productContainer.appendTo($jQueryElement);

        let self = this;

        productBtnAdd.on('click', function () {
            basket.addProduct(self.id, self.price, self.name);
        });
        productBtnDelete.on('click', function () {
            basket.removeProduct(self.id);
        });
    }
}
