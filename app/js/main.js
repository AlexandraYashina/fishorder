$(document).ready(function () {
    let productsContainer = $('#goods');
    let basket = new Basket('basket');

    function getAvailableItems(productsContainer) {
        $.ajax({
            type: 'GET',
            url: './json/available_products.json',
            context: this,
            dataType: 'json',
            success: function (data) {
                let productsItems = [];
                for (let i = 0; i < data.availableProducts.length; i++) {
                    productsItems.push(data.availableProducts[i]);
                    let product = new Good(data.availableProducts[i].id_product, data.availableProducts[i].price, data.availableProducts[i].name, data.availableProducts[i].image);
                    product.render(productsContainer, basket);
                }
            },
            error: function (err) {
                console.log(err.status);
            }
        });
    }
    getAvailableItems(productsContainer);
    $('.smallBasket').on('click', function () {
        basket.render($('.basket_wrapper'));
    });
    basket.render($('.basket_wrapper'));
});