var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoPrefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    BS = require('browser-sync'),
    htmlMin = require('gulp-htmlmin'),
    rename = require('gulp-rename'),
    delFiles = require('del'),
    cssMinify = require('gulp-csso'),
    uglify = require('gulp-uglify-es').default;

gulp.task('test', function () {
    console.log('Gulp works!');
});

gulp.task('html', function () {
    gulp.src(['./app/html/index.html', './app/html/basket.html'])
        .pipe(htmlMin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./dist'));

    BS.reload({
        stream: false
    });
});

gulp.task('json', function () {
    gulp.src(['./app/json/*.json'])
        .pipe(gulp.dest('./dist/json'));

    BS.reload({
        stream: false
    });
});

gulp.task('scripts', function () {
    return gulp.src([ // Берем все необходимые библиотеки
            'app/libs/jquery/dist/jquery.min.js', // Берем jQuery
            'app/libs/jquery-ui/jquery-ui.min.js', //Берем jQueryUI
            'app/libs/bootstrap-4.2.1/dist/js/bootstrap.min.js' //Берем bootstrap
        ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify())
        .pipe(gulp.dest('./app/js')); // Выгружаем в папку app/js
});
gulp.task('js', function () {
    gulp.src('./app/js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/js'));

    BS.reload({
        stream: false
    });
});

gulp.task('img', function () {
    return gulp.src('./app/img/*') // Берем все изображения из app
        .pipe(gulp.dest('./dist/img')); // Выгружаем на продакшен
});


gulp.task('fonts', function () {
    return gulp.src(['./app/fonts/*'])
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('sass', function () {
    gulp.src([
            'app/libs/bootstrap-4.2.1/scss/bootstrap.scss', //bootstrap 
            'app/scss/**/*.scss' // мои стили
        ])
        .pipe(sass())
        .pipe(concat('styles.css')) // Собираем все в кучу в новом файле styles.scss
        .pipe(autoPrefix())
        .pipe(gulp.dest('./dist/css'))
        .pipe(cssMinify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/css'));

    BS.reload({
        stream: false
    });
});

gulp.task('clear', function () {
    delFiles.sync(['./dist/*']);
});

gulp.task('watchFiles', function () {
    gulp.watch(['./app/html/index.html', './app/html/basket.html'], ['html']);
    gulp.watch('./app/js/**/*.js', ['js']);
    gulp.watch('./app/scss/**/*.scss', ['sass']);
});

gulp.task('server', function () {
    BS({
        server: {
            baseDir: './dist'
        }
    });
});

gulp.task('default', ['clear', 'test', 'html', 'js', 'sass', 'scripts', 'img', 'watchFiles', 'server', 'json', 'fonts'], function () {
    console.log('Default task!')
});